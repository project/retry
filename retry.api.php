<?php

/**
 * Describe syncable operations
 *
 * TODO: Write full documentation.
 */
function hook_retry_definitions() {
  $defs = array();

  $defs['vtiger_registration'] = array(
    // Required: a title for this operation
    'title' => 'Sync registration to VTiger',
    // Required: a callback function for executing the operation
    'callback' => '_dummy_sync_registration_to_vtiger',
    // optional: path to a file which should be included before calling the callback function.
    'file' => drupal_get_path('module', 'retry') . '/the_file.inc',
    // optional: maximum number of times this task can be tried.
    'max_tries' => 10,
    // optional: delay between subsequent tries
    'delay_seconds' => 3600 * 10,
  );

  return $defs;
}
