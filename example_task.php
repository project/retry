<?php

/**
 * An example task for the Arjang Re-Sync Platform.
 */
function _retry_example_task($data, $opid) {
  $op = retry_load_operation($opid);

  watchdog('retry', 'Executing the example task with OpID: %opid. Try count until now: %count. Data: %data', array(
    '%opid' => $opid,
    '%count' => $op->try_count,
    '%data' => print_r(unserialize($data), TRUE),
  ));

  return TRUE;
}
